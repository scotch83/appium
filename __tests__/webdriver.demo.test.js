const wdio = require("webdriverio");
const device = require("../capabilities/Android.Galaxy.Nexus.11");

const opts = {
    path: '/wd/hub',
    port: 4723,
    capabilities: {
        platformName: device.platformName,
        platformVersion: device.platformVersion,
        deviceName: device.deviceName,
        app: "c:/projects/appium/ApiDemos-debug.apk",       
        appPackage: "io.appium.android.apis",
        appActivity: ".view.TextFields",
        automationName: "UiAutomator2"
    }
};

let client = null;

// -- One time set-up
beforeAll(async (done) => {
    try {
        client = await wdio.remote(opts);
        done();
    } catch (error) {
        done(error);
    }
});

// -- One time tear-down
afterAll(async (done) => {
    try {
        await client.deleteSession();
        done();
    } catch (error) {
        done(error);
    }
});

// -- Tests
test('Check textfield typing', async (done) => {
    const field = await client.$("android.widget.EditText");
    await field.setValue("Hello World!");
    const value = await field.getText();
    expect(value).toBe("Hello World!");
    done();
});

test('Check textfield typing 2', async (done) => {
    const field = await client.$("android.widget.EditText");
    await field.setValue("Hello World XXX!");
    const value = await field.getText();
    expect(value).toBe("Hello World XXX!");
    done();
});